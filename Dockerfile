FROM python:3.11-slim-bookworm as web

RUN mkdir /app

WORKDIR /app

COPY requirements.txt requirements.txt 

RUN pip install -r requirements.txt

FROM prefecthq/prefect:2-python3.11 as worker

COPY flows/requirements.txt requirements.txt

RUN pip install -r requirements.txt

