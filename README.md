# Toy Local Prefect 2 FastAPI Docker Compose

Has this ever happened to you? You're working on a system that runs [Prefect 2](https://www.prefect.io/) flows and fetches the results. To test your system, you just need a quick and dirty way to fire off a flow and fetch the expected results without having to run the real thing. 

Sure, you could fire up your local [docker-desktop kubernetes](https://docs.docker.com/desktop/kubernetes/), deploy the real flows onto it, and call them, but that's a lot of setup when all you really care to test is that your system can call Prefect and retrieve results. 

I present here a toy docker compose example of a quick and dirty local Prefect 2 setup that just uses a [process pool and worker](https://docs.prefect.io/latest/concepts/work-pools/#worker-types) to run some test prefect flows. In this case, the flow just returns a [Pandas](https://pandas.pydata.org/) dataframe.

The web service is just a small [FastAPI](https://fastapi.tiangolo.com/) application that serves 2 GET endpoints. One that kicks off the fake flow (`/`), and the other retrieves the data (`/{flow_run_id}`.

I hope this helps any wary traveller that is looking for something like this. It actually took me way longer than I thought to figure out how to make these pieces fit together.

## Setup

A single command runs a bash script that deploys our pool and flow deployment:
```bash
docker compose run prefect-worker bash /root/flows/deploy.sh
```

We then just `docker compose up` and your system should be ready to go. You'll be able to view your prefect UI it localhost:4200 and the FastAPI app will be up at localhost:8001
