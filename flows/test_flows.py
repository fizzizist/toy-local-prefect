import pandas as pd
from prefect import flow


@flow(name="return a df", persist_result=True)
def return_a_df(some_param: list[str]):
    return pd.DataFrame([[1, 1], [2, 2], [3, 3]])
