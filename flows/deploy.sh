echo creating worker pool
prefect work-pool create -t process test-process-pool

echo deploying test flow
prefect --no-prompt deploy --name "return a df" --pool test-process-pool /root/flows/test_flows.py:return_a_df
