from fastapi import FastAPI
from prefect import get_client
from uuid import UUID

app = FastAPI()


@app.get("/")
async def index():
    async with get_client() as c:
        deployment = await c.read_deployment_by_name("return a df/return a df")
        flow_run = await c.create_flow_run_from_deployment(
            deployment.id,
            parameters={"some_param": ["hello", "world"]},
        )

    return flow_run.id


@app.get("/{flow_run_id}")
async def get_result(flow_run_id: UUID):
    async with get_client() as c:
        flow_run = await c.read_flow_run(
            flow_run_id,
        )

    results = await flow_run.state.result(fetch=True)

    return results.to_dict("records")
